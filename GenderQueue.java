import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

public class GenderQueue {
    private Deque<Person> queue;
    private int count;
    private int count1;

    public GenderQueue() {
        queue = new ArrayDeque<>();
        count=0;
        count1=1;
    }

    public void addPerson(Person person) {
        if (person.getGender().equalsIgnoreCase("male")) {
            queue.addFirst(person);
            count++;
        } else if (person.getGender().equalsIgnoreCase("female")) {
            queue.addLast(person);
        } else {
            throw new IllegalArgumentException("Gender must be either 'male' or 'female'");
        }
    }

    public Queue<Person> call() {
        ArrayDeque<Person> temp = new ArrayDeque<>();{
        }
        for (Person p : queue) {
            if(count==0){
            break;}
            temp.addFirst(p);
            queue.remove();
            count--;
            }

        return temp;
    }
    public Person callPerson() {
        if (count1%2==1) {
            count1++;
            return call().peek();
        } else {
            count1++;
            return queue.poll();

        }
    }

    public boolean isEmpty() {
        return queue.isEmpty() ;
    }
}
