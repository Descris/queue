
 public class Person {
    private final String name;
    private final String gender;

        public Person(String name, String gender) {
            this.name = name;
            this.gender = gender;
        }

        public String getName() {
            return name;
        }

        public String getGender() {
            return gender;
        }

        @Override
        public String toString() {
            return name + " (" + gender + ")";
        }
    }


